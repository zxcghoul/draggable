var mouseDown = false,
    offsetX, offsetY;


document.getElementById('drag').onmousedown = function(e) {
    mouseDown = true;
    var rect = e.target.getBoundingClientRect();
    offsetX = e.clientX - rect.left;
    offsetY = e.clientY - rect.top;
}

document.getElementById('drag').onmouseup = function(e) {
    mouseDown = false;
}

window.addEventListener('load', (event) => {
    var drag = document.getElementById('drag');
    window.addEventListener('mousemove', (event) => {
        if (mouseDown == true) {
            drag.style.top = event.clientY - offsetY
            drag.style.left = event.clientX - offsetX
        }
    })
});